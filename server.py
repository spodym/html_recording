#!/usr/bin/env python
import base64
import datetime
import json
import os
from collections import defaultdict
from io import BytesIO

from PIL import Image
from tornado import web, ioloop, websocket
from tornado.options import define, options, parse_command_line


CHECK_ORIGIN = True
SOCKET_TIMEOUT = 1000 * 60 * 10  # 10 minutes
TORNADO_PORT = 8090

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


recordings = {}
active_tokens = ['TEST_TOKEN', ]


class RecordingHandler(websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super(RecordingHandler, self).__init__(*args, **kwargs)
        self.token = None

        recordings[self] = {
            'screen': None,
            'mouse': []
        }

    def check_origin(self, origin):
        return CHECK_ORIGIN

    def open(self):
        # This socket will automatically close after 10 minutes.
        self.timeout = ioloop.IOLoop.current().add_timeout(
            datetime.timedelta(milliseconds=SOCKET_TIMEOUT),
            self._close_on_timeout)

    def _close_on_timeout(self):
        if self.ws_connection:
            self.close()

    def on_close(self):
        """Everything is saved on socket close. Whole user activity is
        captured by now.
        """

        path = os.path.join(BASE_DIR, self.token)
        os.makedirs(path, exist_ok=True)

        mmoves_path = os.path.join(path, "mouse.json")
        with open(mmoves_path, "w+") as fp:
            json.dump(recordings[self]['mouse'], fp, indent=4)

        screen = recordings[self]['screenshot']
        img_path = os.path.join(path, "screen.png")
        img = Image.open(BytesIO(base64.b64decode(screen)))
        img.save(img_path)

        del(recordings[self])

    def on_message(self, message):
        message = json.loads(message)

        if message['type'] == 'token':
            self.token = message['data']
            return

        if message['type'] == 'screenshot':
            data = message['data'].split('data:image/png;base64,')[1]
            recordings[self]['screenshot'] = data
            return

        if message['type'] == 'mouse_action':
            recordings[self]['mouse'].append(message)
            return

app = web.Application([
    (r'/recording', RecordingHandler),
])


define("port", default=TORNADO_PORT, help="Tornado port")


if __name__ == '__main__':
    parse_command_line()

    app.listen(options.port)
    ioloop.IOLoop.instance().start()
