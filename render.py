import argparse
import json
import os

import numpy as np
import scipy
from moviepy.editor import ImageSequenceClip
from PIL import Image, ImageDraw


CLIP_FPS = 30


def debug_frame(frame, frame_count=0):
    scipy.misc.imsave('debug_frame_{0:0>3}.jpg'.format(frame_count), frame)


class Renderer(object):
    def __init__(self, recordings_dir):
        super(Renderer, self).__init__()
        self.mousemovements_path = os.path.join(recordings_dir, 'mouse.json')
        self.screenshot_path = os.path.join(recordings_dir, 'screen.png')
        self.mouse_mvt = None

    def load_recordings(self):
        with open(self.mousemovements_path) as fp:
            self.mouse_mvt = json.load(fp)
            self.mouse_mvt = sorted(self.mouse_mvt, key=lambda x: x['date'])

        self.screen = Image.open(self.screenshot_path)

    def render(self, out_path):
        self.load_recordings()

        # Timestamps are in ms
        timestamps = [x['date'] for x in self.mouse_mvt]
        start_ts = min(timestamps)
        duration = max(timestamps) - start_ts
        duration_s = duration / 1000
        frames_count = int(duration_s * CLIP_FPS)

        # FIXME: IDK why this produces video with artifacts.
        # def make_frame(t):
        #     crop = self.screen.crop(box=(0, 0, 1000, 600))
        #     return np.asarray(crop, dtype='uint8')
        # clip = VideoClip(make_frame, duration=duration_s)

        # Append information in which frame movement should appear
        for mv in self.mouse_mvt:
            t_indes_ms = mv['date'] - start_ts
            t_indes_s = t_indes_ms / 1000
            mv['frame'] = int(t_indes_s * CLIP_FPS)

        sequence = []
        mmv_index = 0
        next_mmv = self.mouse_mvt[1]
        for frame_index in range(frames_count):

            # Find most recent mouse position for given frame
            while next_mmv['frame'] <= frame_index:
                mmv_index += 1
                next_mmv = self.mouse_mvt[mmv_index + 1]

            # TODO: Window size is not yet send so I am using fixed size.
            image = self.screen.crop(box=(0, 0, 1000, 600))

            # Add mouse position to frame (red dot)
            draw = ImageDraw.Draw(image)
            current_mmv = self.mouse_mvt[mmv_index]
            x0 = current_mmv['data']['x']
            y0 = current_mmv['data']['y']
            draw.ellipse([x0 - 5, y0 - 5, x0 + 5, y0 + 5], 'red')
            del draw

            # Decode frame to numpy matrix
            frame = np.asarray(image, dtype='uint8')
            sequence.append(frame)

        # Encode frames sequence into video
        clip = ImageSequenceClip(sequence, fps=CLIP_FPS)
        clip.write_videofile(out_path)


def main():
    # Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-d", "--dir", required=True,
        help="Path to the recording folder")
    args = vars(ap.parse_args())

    r = Renderer(args['dir'])
    r.render('out.mp4')


if __name__ == '__main__':
    main()
