var recording_socket = new WebSocket("ws://127.0.0.1:8090/recording");
var recording_token = "TEST_TOKEN"  // This should be recieved from server

function send_data(rec) {
  rec.date = Date.now();
  recording_socket.send(JSON.stringify(rec));
}

var mouse_listener = function(e) {
  let data = {
    "altKey": e.altKey,
    "clientX": e.clientX,
    "clientY": e.clientY,
    "composed": e.composed,
    "ctrlKey": e.ctrlKey,
    "eventPhase": e.eventPhase,
    "isTrusted": e.isTrusted,
    "layerX": e.layerX,
    "layerY": e.layerY,
    "metaKey": e.metaKey,
    "movementX": e.movementX,
    "movementY": e.movementY,
    "offsetX": e.offsetX,
    "offsetY": e.offsetY,
    "pageX": e.pageX,
    "pageY": e.pageY,
    "returnValue": e.returnValue,
    "screenX": e.screenX,
    "screenY": e.screenY,
    "shiftKey": e.shiftKey,
    "type": e.type,
    "x": e.x,
    "y": e.y
  };

  send_data({
    'type': 'mouse_action',
    'data': data
  });
}

var send_screenshot = function() {
  html2canvas(document.body, {
    onrendered: function(canvas) {
      var data = canvas.toDataURL();

      send_data({
        'type': 'screenshot',
        'data': data
      });
    }
  });
}

var send_token = function() {
  send_data({
    'type': 'token',
    'data': recording_token
  });
}

$('document').ready(function(){
  if (recording_token !== undefined) {
    send_screenshot();
    send_token();

    document.addEventListener('click', mouse_listener);
    document.addEventListener('contextmenu', mouse_listener);
    document.addEventListener('dblclick', mouse_listener);
    document.addEventListener('mousemove', mouse_listener);
    document.addEventListener('mousedown', mouse_listener);
    document.addEventListener('mouseup', mouse_listener);

    // TODO: Add scroll data
    // $(window).scrollTop();
    // window.onscroll = function (e) {
    // // called when the window is scrolled.
    // }
  }
});
