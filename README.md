# Recording user actions on site - Proof of concept

## Install requirements

```bash
mkvirtualenv --python=`which python3` html_recording
pip install -r requirements
```

## Run

This is Tornado server for recording data from `test.html`
```bash
./server.py
```

Go to `test.html`
When session will end (Ctrl+R f.e.) all data will be saved into `recordings` dir.

This will render user actions as video file:
```bash
python render.py -d recordings/TEST_TOKEN
```


